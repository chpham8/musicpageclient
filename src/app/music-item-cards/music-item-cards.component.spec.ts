import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MusicItemCardsComponent } from './music-item-cards.component';

describe('MusicItemCardsComponent', () => {
  let component: MusicItemCardsComponent;
  let fixture: ComponentFixture<MusicItemCardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MusicItemCardsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MusicItemCardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
