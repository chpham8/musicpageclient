import {Component, OnInit} from '@angular/core';
import {MusicItemService} from '../music-item.service';
import {GlobalService} from '../global.service';
import {LoginService} from '../login.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-music-item-cards',
  templateUrl: './music-item-cards.component.html',
  styleUrls: ['./music-item-cards.component.css']
})
export class MusicItemCardsComponent implements OnInit {
  items: Array<any>;
  results: Array<any>;
  isAdmin: boolean;
  isUser: boolean;

  constructor(private MusicService: MusicItemService, private router: Router, private Global: GlobalService, private Login: LoginService) {
    this.isAdmin = this.Global.isAdmin;
    this.isUser = this.Global.isUser;
  }

  ngOnInit() {
    this.getItems();
  }

  getItems(): void {
    this.MusicService.getAllItems().subscribe(data => {
      this.items = data;
    });
  }

  search(term: string): void {
    this.MusicService.searchItems(term).subscribe(data => {
      this.MusicService.results = data;
    });
  }

  addToCart(id: number, quantity: number) {
    this.MusicService.getItemForCart(id).subscribe(data => {
      data.quantity = quantity;
      this.MusicService.addToCart(data).subscribe(newdata => {
        this.MusicService.getAllCartItems().subscribe(cartdata => {
          this.Global.resetTotal();
          for (let i of cartdata) {
            this.Global.total += i.price * i.quantity;
            this.Global.addToTotal();
          }
        });
      });
    });
  }

  deleteItem(id: number) {
    this.MusicService.deleteItem(id).subscribe(() => {
      this.getItems();
    });
  }
}
