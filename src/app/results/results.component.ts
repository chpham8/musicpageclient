import { Component, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import {MusicItemService} from '../music-item.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})
export class ResultsComponent implements OnInit, OnChanges {
  results: Array<any>;

  constructor(private MusicService: MusicItemService, private router: Router) { }

  ngOnInit() {
    this.MusicService.searchItems(this.MusicService.term).subscribe(data => {
      this.results = data;
    });
  }

  ngOnChanges() {
    this.MusicService.searchItems(this.MusicService.term).subscribe(data => {
      this.results = data;
    });
  }
}
