import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {MusicItemService} from '../music-item.service';
import {GlobalService} from '../global.service';

@Component({
  selector: 'app-create-item',
  templateUrl: './create-item.component.html',
  styleUrls: ['./create-item.component.css']
})
export class CreateItemComponent implements OnInit {
  newItem: FormGroup;
  itemOb: object = [{}];
  isAdmin: boolean;
  isUser: boolean;

  constructor(private musicService: MusicItemService, private Global: GlobalService) {
    this.isAdmin = this.Global.isAdmin;
    this.isUser = this.Global.isUser;
  }

  ngOnInit() {
    this.newItem = new FormGroup({
      name: new FormControl(),
      description : new FormControl(),
      price: new FormControl(),
      image: new FormControl(),
      type: new FormControl(),
      quantity: new FormControl()
    });
  }
  create() {
    const ob = {
      name : this.newItem.controls.name.value,
      description : this.newItem.controls.description.value,
      price: this.newItem.controls.price.value,
      image: this.newItem.controls.image.value,
      type: this.newItem.controls.type.value,
      quantity: 1
    };
    this.musicService.createItem(ob).subscribe(data => {
      this.itemOb = data;
    });
  }

}
