import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MusicItemService {
  items: Array<any>;
  results: Array<any>;
  term: string;

  constructor(private http: HttpClient) { }

  getAllItems(): Observable<any> {
    return this.http.get('//localhost:8080/items');
  }

  getAllCartItems(): Observable<any> {
    return this.http.get('//localhost:8080/cart');
  }

  getItemForCart(id: number): Observable<any> {
    return this.http.get('//localhost:8080/items/itemId/' + id);
  }

  addToCart(cartItems: any) {
    return this.http.post('//localhost:8080/cart/add', cartItems) as Observable<any>;
  }

  createItem(creation: any) {
    return this.http.post('//localhost:8080/items/add', creation) as Observable<any>;
  }

  emptyCart() {
    return this.http.get('//localhost:8080/cart/empty');
  }

  searchItems(term: string): Observable<any> {
    return this.http.get('//localhost:8080/items/itemName/' + term);
  }

  deleteItem(id: number): Observable<any> {
      return this.http.delete('//localhost:8080/items/delete/' + id);
  }

  removeCartItem(id: number): Observable<any> {
    return this.http.delete('//localhost:8080/cart/remove/' + id);
  }

}
