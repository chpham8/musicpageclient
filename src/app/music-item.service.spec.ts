import { TestBed } from '@angular/core/testing';

import { MusicItemService } from './music-item.service';

describe('MusicItemService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MusicItemService = TestBed.get(MusicItemService);
    expect(service).toBeTruthy();
  });
});
