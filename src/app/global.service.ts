import { Injectable } from '@angular/core';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {
  total: any = 0;
  _observer: Subject<number> = new Subject();
  isAdmin: boolean;
  isUser: boolean;

  get globalTotal(): Subject<number> {
    return this._observer;
  }
  set globalTotal(src: Subject<number>) { }

  constructor() { }

  addToTotal() {
    this._observer.next();
  }

  resetTotal() {
    this.total = 0;
  }

}
