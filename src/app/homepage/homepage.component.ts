import {Component, Input, OnInit} from '@angular/core';
import {MusicItemService} from '../music-item.service';
import {GlobalService} from '../global.service';

@Component({
  selector: 'app-header',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {
  results: Array<any>;
  cartTotal: any = 0;

  constructor(private MusicService: MusicItemService, private Global: GlobalService) {
    this.Global._observer.subscribe(value => {
      this.cartTotal = this.Global.total;
    });
  }

  search(term: string): void {
    this.MusicService.searchItems(term).subscribe(data => {
      this.results = data;
    });
  }

  ngOnInit() { }

}
