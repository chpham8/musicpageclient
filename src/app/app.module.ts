import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MusicItemCardsComponent } from './music-item-cards/music-item-cards.component';
import {HttpClientModule} from '@angular/common/http';
import {MusicItemService} from './music-item.service';
import { HomepageComponent } from './homepage/homepage.component';
import { LoginComponent } from './login/login.component';
import { CartComponent } from './cart/cart.component';
import { BassComponent } from './bass/bass.component';
import { GuitarsComponent } from './guitars/guitars.component';
import { KeyboardsComponent } from './keyboards/keyboards.component';
import { DrumsComponent } from './drums/drums.component';
import { ResultsComponent } from './results/results.component';
import {GlobalService} from './global.service';
import {ModalComponent} from './modal/modal.component';
import { NgbModule, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CreateItemComponent } from './create-item/create-item.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    MusicItemCardsComponent,
    HomepageComponent,
    LoginComponent,
    CartComponent,
    BassComponent,
    GuitarsComponent,
    KeyboardsComponent,
    DrumsComponent,
    ResultsComponent,
    CreateItemComponent,
    ModalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule
  ],
  providers: [MusicItemService, GlobalService, NgbActiveModal],
  bootstrap: [AppComponent],
  exports: [ReactiveFormsModule],
  entryComponents: [ModalComponent]
})
export class AppModule { }
