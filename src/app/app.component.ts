import {Component, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {Router} from '@angular/router';
import {MusicItemService} from './music-item.service';
import {GlobalService} from './global.service';
import {ModalComponent} from './modal/modal.component';
import {NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'Music World';
  cartTotal: any = 0;
  isAdmin: boolean;
  isUser: boolean;

  private searchTerms = new Subject<string>();

  constructor(private MusicService: MusicItemService, private router: Router, private Global: GlobalService, private modalService: NgbModal) {
    this.Global._observer.subscribe(value => {
      this.cartTotal = this.Global.total;
      this.isAdmin = this.Global.isAdmin;
      this.isUser = this.Global.isUser;
    });
  }

  ngOnInit() {
    this.MusicService.getAllCartItems().subscribe(data => {
      this.Global.resetTotal();
      for (let i of data) {
        this.Global.total += (i.price * i.quantity);
      }
      this.cartTotal = this.Global.total;
    });
  }

  search(term: string): void {
    if (term === '') {
      this.MusicService.term = '';
    } else {
      this.MusicService.term = term;
      this.router.navigate(['/results']);
    }
  }

  open() {
    const modalRef = this.modalService.open(ModalComponent, {size: 'lg'});
    modalRef.componentInstance.title = 'Your Cart';
  }
}
