import { Component, OnInit } from '@angular/core';
import {GlobalService} from '../global.service';
import {LoginService} from '../login.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  isAdmin: boolean;
  isUser: boolean;

  constructor(private Global: GlobalService, private Login: LoginService, private router: Router) {
    this.isAdmin = this.Global.isAdmin;
    this.isUser = this.Global.isUser;
  }

  ngOnInit() {
  }

  submitLogin(uname: string, pw: string) {
    this.Login.checkUser(uname).subscribe(data => {
      for (let user of data) {
        if (uname === user.username && pw === user.password) {
          if (user.role === 'admin') {
            this.isAdmin = true;
            this.Global.isAdmin = this.isAdmin;
          } else if (user.role === 'user') {
            this.isUser = true;
            this.Global.isUser = this.isUser;
          }
          this.router.navigate(['/homepage']);
        } else {
          alert('Username/password not found');
        }
      }
      this.Global.addToTotal();
    });

  }

}
