import { Component, OnInit } from '@angular/core';
import {MusicItemService} from "../music-item.service";
import {GlobalService} from "../global.service";

@Component({
  selector: 'app-drums',
  templateUrl: './drums.component.html',
  styleUrls: ['./drums.component.css']
})
export class DrumsComponent implements OnInit {
  items: Array<any>;
  constructor(private MusicService: MusicItemService, private Global: GlobalService) { }

  addToCart(id: number, quantity: number) {
    this.MusicService.getItemForCart(id).subscribe(data => {
      data.quantity = quantity;
      this.MusicService.addToCart(data).subscribe(newdata => {
        this.MusicService.getAllCartItems().subscribe(cartdata => {
          this.Global.resetTotal();
          for (let i of cartdata) {
            this.Global.total += i.price * i.quantity;
            this.Global.addToTotal();
          }
        });
      });
    });
  }

  ngOnInit() {
    this.MusicService.getAllItems().subscribe(data => {
      this.items = data;
    });
  }

}
