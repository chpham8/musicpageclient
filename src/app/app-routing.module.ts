import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {GuitarsComponent} from "./guitars/guitars.component";
import {BassComponent} from "./bass/bass.component";
import {KeyboardsComponent} from "./keyboards/keyboards.component";
import {DrumsComponent} from "./drums/drums.component";
import {HomepageComponent} from "./homepage/homepage.component";
import {CartComponent} from './cart/cart.component';
import {LoginComponent} from './login/login.component';
import {ResultsComponent} from './results/results.component';
import {CreateItemComponent} from './create-item/create-item.component';

const routes: Routes = [
  { path: 'homepage', component: HomepageComponent},
  { path: 'guitars', component: GuitarsComponent},
  { path: 'bass', component: BassComponent},
  { path: 'keyboards', component: KeyboardsComponent},
  { path: 'drums', component: DrumsComponent},
  { path: 'login', component: LoginComponent},
  { path: 'cart', component: CartComponent},
  { path: 'results', component: ResultsComponent},
  { path: 'create', component: CreateItemComponent},
  { path: '', redirectTo: '/homepage', pathMatch: 'full'}];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
