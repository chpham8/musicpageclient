import { Component, OnInit } from '@angular/core';
import {MusicItemService} from '../music-item.service';
import {GlobalService} from '../global.service';
import { Router } from '@angular/router';
import {NgbModal, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  carts: any;
  cartTotal: any;

  constructor(private MusicService: MusicItemService, private router: Router, private Global: GlobalService, public activeModal: NgbActiveModal) {
    this.Global._observer.subscribe(value => {
      this.cartTotal = this.Global.total;
    });
  }

  ngOnInit() {
    this.getCart();
  }

  getCart() {
    this.MusicService.getAllCartItems().subscribe(data => {
      this.carts = data;
      this.Global.resetTotal();
      for (let i of this.carts) {
        this.Global.total += (i.price * i.quantity);
      }
      this.cartTotal = this.Global.total;
      this.Global.addToTotal();
    });
  }

  removeFromCart(id: number) {
    this.MusicService.removeCartItem(id).subscribe(() => {
      this.getCart();
    });
  }

  completeOrder() {
    this.MusicService.emptyCart().subscribe();
    console.log(this.carts);
    this.router.navigate(['/homepage']);
    this.activeModal.dismiss();
    this.Global.resetTotal();
    this.Global.addToTotal();
  }
}
